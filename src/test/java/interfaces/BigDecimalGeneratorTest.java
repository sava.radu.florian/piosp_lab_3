package interfaces;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import java.io.File;

class BigDecimalGeneratorTest {

    @Test
    void TestReadingAndWriting() {
        BigDecimalGenerator.writeToFile("testfile.txt", 100);
        BigDecimalGenerator.readFromFile("testfile.txt", 100).forEach(Assertions::assertNotNull);
        File file = new File("testfile.txt");
        Assertions.assertTrue(file.delete());
    }
}
