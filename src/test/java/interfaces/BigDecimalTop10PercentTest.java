package interfaces;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.IntStream;

public class BigDecimalTop10PercentTest {
    private List<BigDecimal> bigDecimals = null;

    @BeforeEach
    void init(){
        bigDecimals = IntStream.range(0, 100).mapToObj(BigDecimal::new).toList();
    }

    @Test
    void TestTop10Percent(){
        BigDecimalTop10Percent top10Percent = (list) -> list.stream().sorted().skip(list.size()* 9L /10).toList();
        BigDecimalTop10Percent.serialize(top10Percent, "top10percent.ser");
        int initial = 90;
        List<BigDecimal> newBigDecimals = BigDecimalTop10Percent.deserialize("top10percent.ser").calculate(bigDecimals);
        for(int i=0;i<10;i++){
            Assertions.assertEquals(newBigDecimals.get(i), new BigDecimal(initial));
            initial++;
        }
    }
}
