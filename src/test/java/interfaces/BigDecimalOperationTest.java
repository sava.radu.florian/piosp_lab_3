package interfaces;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import java.util.List;
import java.util.stream.IntStream;

public class BigDecimalOperationTest {

    private List<BigDecimal> bigDecimals = null;

    @BeforeEach
    void init(){
        bigDecimals = IntStream.range(0, 100).mapToObj(BigDecimal::new).toList();
    }

    @Test
    void TestSum(){
        BigDecimalOperation sum = (list) ->list.stream().reduce(BigDecimal.ZERO,BigDecimal::add);
        BigDecimalOperation.serialize(sum, "sum.ser");
        Assertions.assertEquals(BigDecimalOperation.deserialize("sum.ser").calculate(bigDecimals), new BigDecimal(4950));

    }

    @Test
    void TestAverage(){
        BigDecimalOperation average = (list) -> list.stream().reduce(BigDecimal.ZERO,BigDecimal::add)
                .divide(new BigDecimal(list.size()));
        BigDecimalOperation.serialize(average, "average.ser");
        Assertions.assertEquals(BigDecimalOperation.deserialize("average.ser").calculate(bigDecimals), new BigDecimal(49.5));

    }
}
