package interfaces;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.IntStream;

public class BigDecimalSerializerTest {
    private List<BigDecimal> bigDecimals = null;

    @BeforeEach
    void init(){
        bigDecimals = IntStream.range(0, 100).mapToObj(BigDecimal::new).toList();
    }

    @Test
    void TestSerialization(){
        BigDecimalSerializer.serialize(bigDecimals, "bigDecimals.ser");
        List<BigDecimal> newBigDecimals = BigDecimalSerializer.deserialize("bigDecimals.ser", 100);
        for(int i=0;i<100;i++){
            Assertions.assertEquals(newBigDecimals.get(i), bigDecimals.get(i));
        }
    }
}
