import interfaces.BigDecimalGenerator;
import interfaces.BigDecimalOperation;
import interfaces.BigDecimalSerializer;
import interfaces.BigDecimalTop10Percent;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


class Main {
    public static void main(String[] args) {

        // first, we generate our numbers in a file and then read them in our list
        BigDecimalGenerator.writeToFile("manyNumbers.txt", 100000);
        List<BigDecimal> bigDecimals = BigDecimalGenerator.readFromFile("manyNumbers.txt", 100000);

        // each of our elements is serialized in a file and then deserialized,
        // we create a new list containing the same elements
        System.out.println("Serializing bigDecimals");
        BigDecimalSerializer.serialize(bigDecimals, "bigDecimals.ser");
        System.out.println("Deserializing bigDecimals");
        bigDecimals = BigDecimalSerializer.deserialize("bigDecimals.ser", 100000);

        // we generate our sum lambda
        BigDecimalOperation sum = (list) ->list.stream().reduce(BigDecimal.ZERO,BigDecimal::add);


        // our lambda is serialized and then read again after being deserialized, then used to compute the actual sum
        System.out.println("Serializing sum");
        BigDecimalOperation.serialize(sum, "sum.ser");
        System.out.println("Deserializing sum");
        BigDecimal computedSum =  BigDecimalOperation.deserialize("sum.ser").calculate(bigDecimals);

        // we print our sum
        System.out.println("\n\nSum:");
        System.out.println(computedSum);

        // we generate our average lambda
        BigDecimalOperation average = (list) -> list.stream().reduce(BigDecimal.ZERO,BigDecimal::add)
                .divide(new BigDecimal(list.size()));

        // our lambda is serialized and then read again after being deserialized, then used to compute the actual average
        System.out.println("Serializing average");
        BigDecimalOperation.serialize(average,"average.ser");
        System.out.println("Deserializing average");
        BigDecimal computedAverage = BigDecimalOperation.deserialize("average.ser").calculate(bigDecimals);

        // we print our sum
        System.out.println("\n\nAverage:");
        System.out.println(computedAverage);

        // we generate our top 10 percent lambda
        BigDecimalTop10Percent top10Percent = (list) -> list.stream().sorted().skip(list.size()* 9L /10).toList();

        // our lambda is serialized and then read again after being deserialized,
        // then used to compute the actual top 10 percent
        System.out.println("Serializing top 10%");
        BigDecimalTop10Percent.serialize(top10Percent, "top10percent.ser");
        List<BigDecimal> computedTop10percent = BigDecimalTop10Percent.deserialize("top10percent.ser").calculate(bigDecimals);
        System.out.println("Deserializing top 10%");

        // we print our top 10 percent
        System.out.println("\n\nTop 10 percent:");
        computedTop10percent.forEach(System.out::println);


    }
}

