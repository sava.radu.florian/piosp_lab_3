package interfaces;

import java.io.*;
import java.math.BigDecimal;
import java.util.List;
/**
 *  this interface is used in order to store a reducer lambda which returns a BigDecimal
 *  it can also serialize and deserialize to file its objects
 */
@FunctionalInterface
public interface BigDecimalOperation extends Serializable {
    /**
     * @param list a list of BigDecimal objects
     * @return a BigDecimal object
     */
    BigDecimal calculate(List<BigDecimal> list);

    /**
     * @param lambda the lambda which we want to serialize and add to file
     * @param outputFileName the name of the file where the lambda should be written
     */
    static void serialize(BigDecimalOperation lambda, String outputFileName){
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(outputFileName);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(lambda);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param inputFileName the name of the file from where we want to extract our lambda
     * @return the deserialized lambda
     */
    static BigDecimalOperation deserialize(String inputFileName){
        try {
            FileInputStream fileInputStream = new FileInputStream(inputFileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            BigDecimalOperation bigDecimalOperation= (BigDecimalOperation) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            return bigDecimalOperation;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
