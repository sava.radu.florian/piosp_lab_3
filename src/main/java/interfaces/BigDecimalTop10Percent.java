package interfaces;

import java.io.*;
import java.math.BigDecimal;
import java.util.List;

/**
 *  this interface is used in order to store a filter lambda which returns a list of BigDecimals
 *  it can also serialize and deserialize to file its objects
 */
@FunctionalInterface
public interface BigDecimalTop10Percent extends Serializable {
    /**
     * @param list a list of BigDecimal objects
     * @return a new of BigDecimal objects
     */
    List<BigDecimal> calculate(List<BigDecimal> list);

    /**
     * @param lambda the lambda which we want to serialize and add to file
     * @param outputFileName the name of the file where the lambda should be written
     */
    static void serialize(BigDecimalTop10Percent lambda, String outputFileName){
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(outputFileName);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(lambda);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param inputFileName the name of the file from where we want to extract our lambda
     * @return the deserialized lambda
     */
    static BigDecimalTop10Percent deserialize(String inputFileName){
        try {
            FileInputStream fileInputStream = new FileInputStream(inputFileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            BigDecimalTop10Percent bigDecimalTop10Percent = (BigDecimalTop10Percent) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            return bigDecimalTop10Percent;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}

