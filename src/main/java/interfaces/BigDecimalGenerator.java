package interfaces;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *  this class is used to generate a random list of BigDecimal object, write it to file and read it
 */
public interface BigDecimalGenerator {
    /**
     * @param fileName the name of the file where we want to write our numbers
     * @param howMany how many numbers we want to generate
     */
    static void writeToFile(String fileName, int howMany){
        try {
            FileWriter fileWriter = new FileWriter(fileName, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for(int i=0;i< howMany;i++) {
                BigDecimal randomBigDecimal = BigDecimal.ZERO.add(new BigDecimal(Math.random()).multiply(BigDecimal.TEN
                        .multiply(BigDecimal.TEN).multiply(BigDecimal.TEN).multiply(BigDecimal.TEN).multiply(BigDecimal.TEN)));
                bufferedWriter.append(randomBigDecimal.toString());
                bufferedWriter.append("\n");
            }
            bufferedWriter.close();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param fileName the name of the file from where we read our numbers
     * @param howMany how many numbers we want to read
     * @return a list containing the numbers we read form file as BigDecimal objects
     */
    static List<BigDecimal> readFromFile(String fileName, int howMany){
        try {
            List<BigDecimal> bigDecimals = new ArrayList<>();
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader= new BufferedReader(fileReader);
            for(int i=0;i< howMany;i++) {
                bigDecimals.add(new BigDecimal(bufferedReader.readLine())) ;
            }
            bufferedReader.close();
            fileReader.close();
            return bigDecimals;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
