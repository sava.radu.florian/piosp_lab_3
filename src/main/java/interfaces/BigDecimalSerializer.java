package interfaces;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *  this interface is used to serialize and deserialize to a file a list of BigDecimal objects
 */
public interface BigDecimalSerializer{
    /**
     * @param bigDecimals a list of BigDecimals
     * @param outputFileName the name of the file where the bigDecimals should be serialized
     */
    static void serialize(List<BigDecimal> bigDecimals, String outputFileName){
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(outputFileName);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            for (BigDecimal bigDecimal : bigDecimals) {
                objectOutputStream.writeObject(bigDecimal);
            }
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param inputFileName the name of the file from where we get our serialized BigDecimal variables
     * @param count the number of element we want to extract
     * @return a list containing count objects of type BigDecimal
     */
    static List<BigDecimal> deserialize(String inputFileName, int count){
        try {
            List<BigDecimal> bigDecimals = new ArrayList<>();
            FileInputStream fileInputStream = new FileInputStream(inputFileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            while (count > 0){
                bigDecimals.add((BigDecimal) objectInputStream.readObject());
                count --;
            }
            objectInputStream.close();
            fileInputStream.close();
            return bigDecimals;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
